# More Passwords!

## moe

Maybe already cracked

```txt
UXoa92ZxZimus
```

nevermind is des

password: these

## larry

```txt
$1$mZK.ozbi$R5Lx7FUTAfay.ekJjBxYB0
```

md5
salt: mZK.ozbi
hash: R5Lx7FUTAfay.ekJjBxYB0

password: passwords

## shemp

```txt
$5$shatwofivesix$prxbTSVvLc.IazwHBLafrG0WYEHzRr1Y.vRApCfJrN.
```

password: are

sha256

## curley

```txt
X92vOppkAh9CLedTmUnL3.QRxZ05uwQChkjGzYxtcN6$5$outoforder$
```

should be

```txt
$5$outoforder$X92vOppkAh9CLedTmUnL3.QRxZ05uwQChkjGzYxtcN6
```

password: downright

## joe

```txt
/W3FtaCO5ZFeEMvj4EZloCv6ZiMreqH1Crffji3aA1Y1tlhegNk73DHWxOzm.UF3dn8YYJSqNFB//vT7EszBwS$sdrawkcab$6$
```

backwards, should be

```txt
$6$backwards$SwBzsE7Tv//BFNqSJYY8nd3FU.mzOxWHD37kNgehlt1Y1Aa3ijffrC1HqerMiZ6vColZE4jvMEeFZ5OCatF3W/
```

password: atrocious

## flag

flag is thesepasswordsaredownrightatrocious
